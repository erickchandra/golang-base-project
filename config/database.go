package config

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

// DB for db connection
var DB *gorm.DB

// InitDB initiate connection with db
func initDB() {
	var err error
	dbConf := Config.DbConf
	connString := fmt.Sprintf("%s:%s@%s/%s?charset=utf8&parseTime=True&loc=Local", dbConf.user, dbConf.password, dbConf.host, dbConf.dbName)
	fmt.Println(connString)
	DB, err =  gorm.Open("mysql", connString)
	if err != nil {
		panic(err)
	}
	fmt.Println("success init db connection")
}
