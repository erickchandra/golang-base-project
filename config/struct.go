package config

type databaseConfig struct{
	user string
	password string
	host string
	dbName string
}