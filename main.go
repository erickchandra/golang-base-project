package main

import (
	"net/http"
	"golang-base-project/router"
	"golang-base-project/config"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	config.InitConfig()
	router := router.NewRouter()
	router.SetRouter()

	http.ListenAndServe(":8080", router.Routes)
}