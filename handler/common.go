package handler

import (
	"fmt"
	"errors"
	"encoding/json"
	"net/http"
)

// HealthCheckResponse ...
type HealthCheckResponse struct {
	Message string
}

// HealthCheck handling healthcheck
func HealthCheck(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(&HealthCheckResponse{
		Message: "health check success",
	})

	w.Header().Set("Content-Type", "application/json")
}

func decodeRequestBody(r *http.Request, bucket interface{}) error{
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(bucket)
	if err != nil {
		fmt.Printf("failed to decode request : %+v", r);
		return errors.New("INVALID_REQUEST_BODY")
	}
	fmt.Printf("success decode request from : %+v", bucket);
	return nil
}