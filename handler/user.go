package handler

import (
	"fmt"
	"encoding/json"
	"net/http"
	"golang-base-project/app/logic/user"
)


// CreateUser handler for creating user
func CreateUser(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("\nPOST request to create user\n")

	w.Header().Set("Content-Type", "application/json")

	var request user.CreateUserRequest
	err :=  decodeRequestBody(r, &request)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(err)
		return
	}

	resp, err := user.CreateUser(&request)
	if err != nil {
		fmt.Printf("\nError while create user, %+v\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	json.NewEncoder(w).Encode(resp)
}