package orm

// User for user
type User struct {
	BaseModel
	Name string `gorm:"column:name"`
	Email string `gorm:"column:email"`
	PhoneNumber string `gorm:"column:phone_number"`
}
