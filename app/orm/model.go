package orm

import (
	"time"
)

// BaseModel gorm.Model definition
type BaseModel struct {
  ID   int    `gorm:"column:id; PRIMARY_KEY" json:"id"`
  CreatedAt time.Time `gorm:"column:created_at"`
  UpdatedAt time.Time `gorm:"column:updated_at"`
  DeletedAt *time.Time `gorm:"column:deleted_at"`
}

// BeforeCreate ...
func (b *BaseModel) BeforeCreate() (err error) {
  b.CreatedAt = time.Unix(time.Now().Unix(), 0) // Calculate without nanosec
  return
}

// BeforeUpdate ...
func (b *BaseModel) BeforeUpdate() (err error) {
  b.UpdatedAt = time.Unix(time.Now().Unix(), 0) // Calculate without nanosec
  return
}