package user

import (
	"fmt"
	"golang-base-project/app/orm"
	"golang-base-project/config"
)

// CreateUser logic to create user
func CreateUser(request *CreateUserRequest) (*orm.User, error){
	user := orm.User{
		Name: request.Name,
		Email: request.Email,
		PhoneNumber: request.PhoneNumber,
	}

	if err := config.DB.Save(&user).Error; err != nil {
		fmt.Printf("\nError while saving user data to db, err : %+v", err)
		return nil, err
	}

	fmt.Printf("\nsuccess created user, detail : %+v", user)

	return &user, nil
}