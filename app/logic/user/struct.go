package user

// CreateUserRequest ...
type CreateUserRequest struct {
	Name string `json:"name"`
	Email string `json:"email"`
	PhoneNumber string `json:"phoneNumber"`
}