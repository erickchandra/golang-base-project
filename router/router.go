package router

import (
	"github.com/gorilla/mux"
	handler "golang-base-project/handler"
)

// Impl ...
type Impl struct {
	Routes *mux.Router
}

// NewRouter ...
func NewRouter() *Impl {
	return &Impl{}
}

// SetRouter ...
func (r *Impl)SetRouter() {
	r.Routes = mux.NewRouter()
	r.Routes.HandleFunc("/health_check", handler.HealthCheck)
	r.Routes.HandleFunc("/user", handler.CreateUser).Methods("POST")
}
